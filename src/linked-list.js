const Node = require('./node');

class LinkedList {
    constructor() {
        this.length = 0;
    }

    append(data) {
        var newNode = new Node(data);
        if (this.length === 0) {
            this._head = newNode;
            this._tail = newNode;
        } else {
            newNode.prev = this._tail;
            this._tail.next = newNode;
            this._tail = newNode;
        }
        this.length++;
        return this;
    }

    head() {
        return this._head.data;
    }

    tail() {
        return this._tail.data;
    }

    at(index) {
        var node = this._head;
        if (index < 0 || index >= this.length || this.length === 0) {
            throw 'Index out of bounds';
        } else {
            if (node.next != undefined) {
                for (var i = 0; i < index; i++) {
                    node = node.next;
                }
            } else {
                for (var i = index - 1; i >= 0; i--) {
                    node = node.prev;
                }
            }

        }
        return node.data;
    }

    insertAt(index, data) {
        var node = this._head;
        var next;
        var prev;
        if (this.length <= 1) {
            next = null;
            prev = null;
        } else {
            for (var i = 0; i < index; i++) {
                if (index - i === 1) {
                    prev = node;
                    next = node.next; 
                }
                node = node.next;
            }
        }
        node.prev = prev;
        node.next = next;
        node.data = data;
        return this;
    }

    isEmpty() {
        return this.length === 0;
    }

    clear() {
        var clear = new Node();
        this._head = clear;
        this._tail = clear;
        this.length = 0;
        return this;
    }

    deleteAt(index) {
        var node = this._head;
        var nextNode;
        var prevNode;
        if (index < 0 || index >= this.length || this.length === 0) {
            throw 'Index out of bounds';
        } else {
            if (this.length <= 1) {
                node = node;
            } else {
                for (var i = 0; i < index; i++) {
                    node = node.next;
                    if (index - i === 1) {
                        nextNode = node;
                        prevNode = node.prev.prev;
                    }
                }
                nextNode.prev = prevNode;
                prevNode.next = nextNode;
            }
            node = null;
        }
        this.length--;
        return this;
    }

    reverse() {
        var head = this._tail;
        var tail = this._head;
        for (var i = 0; i < this.length; i++) {
            if (i === 0) {
                head = head
                tail = tail
            } else {
                head = head.prev
                tail = tail.next
            }
        }
        this._head = tail;
        this._tail = head;
        return this;
    }

    indexOf(data) {
        var current = this._head;
        for (var i = 0; i < this.length; i++) {
            if (current.data === data) {
                return i;
            } else {
                current = current.next;
            }
        }
        return -1;
    }
}

module.exports = LinkedList;
